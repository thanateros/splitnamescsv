package main

import (
	"bufio"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
)

var lineNo = 0
var suffixes = []string{"JR", "JR.", "jr", "jr.", "SR", "SR.", "sr", "sr.", "I", "II", "III", "IV"}
var (
	firstName  string
	middleName string
	lastName   string
	suffix     string
)

func contains(ele string, list []string) bool {
	for _, val := range list {
		if val == ele {
			return true
		}
	}
	return false
}

func main() {

	splitnamesCSV := [][]string{}
	outputRow := []string{}

	inputCSVFilePtr := flag.String("file", "", "path to input CSV file that needs second column split into First Name, Middle Name, Last Name, and Suffix columns in output file.\nOutput file has '_split' at end of file name.\nExample: 'splitnamesCSV -file path/to/csv/file'.")
	flag.Parse()

	outputCSV := strings.TrimSuffix(*inputCSVFilePtr, filepath.Ext(*inputCSVFilePtr)) + "_split.csv"
	csvFile, _ := os.Open(*inputCSVFilePtr)
	defer csvFile.Close()

	reader := csv.NewReader(bufio.NewReader(csvFile))

	for {
		line, err := reader.Read()

		if err == io.EOF {
			break
		} else if err != nil {
			fmt.Printf("\nThere was an error: %v\n", err)
			fmt.Println("Aborting. \nType 'splitnamesCSV -h' for assistance.")
			os.Exit(1)
		}

		for i := 0; i < len(line); i++ {
			// 	outputRow = append(outputRow, line[i])
			// }
			// outputRow = append(outputRow, tmp)
			if i == 1 {
				tmp := strings.Replace(line[i], ",", "", -1)
				n := strings.Split(tmp, " ")
				if len(n) == 4 {
					if contains(n[3], suffixes) {
						outputRow = append(outputRow, n[0], n[1], n[2], n[3])
					} else {
						l := []string{}
						l = append(l, n[2], n[3])
						outputRow = append(outputRow, n[0], n[1], strings.Join(l, " "), "")
					}
				}
				if len(n) == 3 {
					if contains(n[2], suffixes) {
						outputRow = append(outputRow, n[0], "", n[1], n[2])
					} else {
						outputRow = append(outputRow, n[0], n[1], n[2], "")
					}
				}
				if len(n) == 2 {
					outputRow = append(outputRow, n[0], "", n[1], "")
				}
				if len(n) == 1 {
					outputRow = append(outputRow, n[0], "", "", "")
				}
				if len(n) == 0 {
					fmt.Printf("NOTE !!! Split name field contains no name !!! \t @ LINE: %v (0-based indexing)\n", lineNo)
				}
				if len(n) > 4 {
					fmt.Printf("NOTE !!! Split name field contains more than 4 sections !!! \t @ LINE: %v (0-based indexing)\n", lineNo)
				}
			} else {
				outputRow = append(outputRow, line[i])
			}
		}

		splitnamesCSV = append(splitnamesCSV, outputRow)
		outputRow = []string{}

		lineNo++
	}

	fmt.Println("\nFinished splitting second column (name column).\nColumn split into 4 columns: First, Middle, Last, Suffix.\nPreparing to write new file...")

	outputFile, err := os.Create(outputCSV)
	checkError("Cannot create file: ", err)
	defer outputFile.Close()

	writer := csv.NewWriter(outputFile)
	defer writer.Flush()

	for _, value := range splitnamesCSV {
		err := writer.Write(value)
		checkError("Cannot write to file: ", err)
	}

	fmt.Printf("==========\nCompleted.\nNew file generated: %v\n==========\n", outputCSV)
}

func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}
