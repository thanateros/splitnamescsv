# splitnamesCSV

A small command-line utility to consume a CSV file exported from a source 
(like a database) where the second column contains a full name that needs to 
be split into parts: First, Middle, Last, and Suffix.  The full name field
may contain any combination of inclusion: just the first name, just the 
first name and last name, just the first, last, and suffix, etc ...; missing 
parts will filled with a blank string.  All other columns pass through
unaltered.  The original file is not altered. The output is a new CSV file
with `_split` after the file name (e.g., `sample.csv` outputs to `sample_split.csv`
in the same folder).

## TL;DR

```bash
$ ls
sample.csv
$ splitnamesCSV -file sample.csv

Finished splitting second column (name column).
Column split into 4 columns: First, Middle, Last, Suffix.
Preparing to write new file...
==========
Completed.
New file generated: sample_split.csv
==========

$ls
sample.csv sample_split.csv
```

## Get the software

```go
$ go get gitlab.com/thanateros/splitnamescsv
```

## Install the software

```bash
$ cd $GOPATH/src/gitlab.com/thanateros/splitnamescsv
$ go install
```

## Use the software

The command takes one flag: `-file path/to/csv/file`.

It takes the input file and passes each field to the output file save that the 
second column it splits into 4 fields (first name, middle name, last name, and 
suffix) and intelligently figures out what is which (and which to leave blank) 
and writes a new CSV to the same source folder with a modified name -- it has 
`_split` after the original file name, e.g., `sample.csv` input file generates 
an output file called `sample_split.csv`. It does not alter the source CSV file.

## Inspiration 

I needed to migrate data from an old RDBMS to a new RDBMS via CSV export and
import. The old database had a single name field for the entire name (which 
may not have every name part filled in) and the new database has the four
name fields (first, middle, last, suffix) thus the need for this particular 
data munging.

**Go** and the good 'ol **strings** library to the rescue.

Enjoy. Play. Improve. 